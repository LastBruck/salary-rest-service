import uvicorn
from fastapi import FastAPI, APIRouter

from salary_service.routers.handler import create_user_router
from salary_service.routers.login import login_router
from salary_service.routers.router_salary_service import salary_router

app = FastAPI(title="salary-rest-service")

main_api_router = APIRouter()

main_api_router.include_router(create_user_router, prefix="/create-user", tags=["create-user"])
main_api_router.include_router(login_router, prefix="/login", tags=["login"])
main_api_router.include_router(salary_router, prefix="/salary", tags=["salary"])
app.include_router(main_api_router)

if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=8000)


