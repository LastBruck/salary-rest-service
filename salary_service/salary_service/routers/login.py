from datetime import timedelta

from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi import status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.ext.asyncio import AsyncSession

import settings
from salary_service.hash_method import Hasher
from salary_service.models import TokenModel
from salary_service.routers.handler import ModelMethod
from salary_service.session import get_session
from salary_service.security import create_access_token

login_router = APIRouter()


async def get_user_by_username_for_auth(username: str, session: AsyncSession):
    async with session.begin():
        user_meth = ModelMethod(session)
        return await user_meth.get_user_by_username(
            username=username,
        )


async def authenticate_user(username: str, password: str, db: AsyncSession):
    user = await get_user_by_username_for_auth(username=username, session=db)
    if user is None:
        return
    if not Hasher.verify_password(password, user.password):
        return
    return user


@login_router.post("/token", response_model=TokenModel)
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(), db: AsyncSession = Depends(get_session)
):
    user = await authenticate_user(form_data.username, form_data.password, db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={'sub': user.username, 'other_data': [1, 2, 3]},
        expires_delta=access_token_expires,
    )
    return {'access_token': access_token, 'token_type': 'bearer'}
