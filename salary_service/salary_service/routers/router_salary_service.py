from datetime import timedelta

from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi import status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

import settings

from salary_service.models import User
from salary_service.routers.handler import ModelMethod
from salary_service.routers.login import get_user_by_username_for_auth
from salary_service.session import get_session

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login/token")


salary_router = APIRouter()


async def get_current_user_from_token(token: str = Depends(oauth2_scheme), db: AsyncSession = Depends(get_session)):
    raise_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Could not validate credentials',
    )
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
        )
        username: str = payload.get('sub')
        if username is None:
            raise raise_exception
    except JWTError:
        raise raise_exception
    user = await get_user_by_username_for_auth(username=username, session=db)
    if user is None:
        raise raise_exception
    return user


async def get_salary_by_user_id(user_id: int, session):
    async with session.begin():
        model_meth = ModelMethod(session)
        salary = await model_meth.get_salary_by_user_id(user_id=user_id)
        if salary:
            salary_amount = salary.amount
            last_promotion = salary.last_promotion
            return salary_amount, last_promotion


@salary_router.get('/')
async def get_salary(user: User = Depends(get_current_user_from_token), db: AsyncSession = Depends(get_session)):
    try:
        salary_amount, last_promotion = await get_salary_by_user_id(user_id=user.id, session=db)
        response = {
            'salary_amount': salary_amount,
            'next_promotion': last_promotion + timedelta(days=365)
        }
        return response
    except IntegrityError as err:
        raise HTTPException(status_code=503, detail=f"Database error: {err}")
