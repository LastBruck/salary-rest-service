from datetime import datetime

from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from salary_service.hash_method import Hasher
from salary_service.models import User, UserModel, ShowUser, Salary
from salary_service.session import get_session

create_user_router = APIRouter()


class ModelMethod:
    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    async def get_user_by_username(self, username: str):
        query = select(User).where(User.username == username)
        res = await self.db_session.execute(query)
        user_row = res.fetchone()
        if user_row is not None:
            return user_row[0]

    async def get_salary_by_user_id(self, user_id: int):
        query = select(Salary).where(Salary.user_id == user_id)
        res = await self.db_session.execute(query)
        salary_row = res.fetchone()
        if salary_row is not None:
            return salary_row[0]


async def create_new_user(body: UserModel, session):
    async with session.begin():
        user = User(
            username=body.username,
            password=Hasher.get_password_hash(body.password)
        )
        salary = Salary(
            amount=body.amount,
            last_promotion=datetime(
                year=body.last_promotion.year,
                month=body.last_promotion.month,
                day=body.last_promotion.day
            )
        )
        user.salary = salary
        session.add(user)
        await session.flush()
        return ShowUser(
            id=user.id,
            username=user.username,
            amount=salary.amount,
            last_promotion=salary.last_promotion
        )


@create_user_router.post("/", response_model=ShowUser)
async def create_user(body: UserModel, db: AsyncSession = Depends(get_session)) -> ShowUser:
    try:
        return await create_new_user(body, db)
    except IntegrityError as err:
        raise HTTPException(status_code=503, detail=f"Database error: {err}")
