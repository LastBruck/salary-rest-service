from pydantic import BaseModel
from sqlalchemy import Column, String, Integer, ForeignKey, DateTime, Float

from sqlalchemy.orm import declarative_base, relationship


Base = declarative_base()


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, nullable=False)
    username = Column(String, nullable=False)
    password = Column(String, nullable=False)
    salary = relationship("Salary", uselist=False, back_populates="user")


class Salary(Base):
    __tablename__ = "salaries"

    id = Column(Integer, primary_key=True, nullable=False)
    amount = Column(Float)
    last_promotion = Column(DateTime)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", back_populates="salary")


class TunedModel(BaseModel):
    class Config:
        orm_mode = True


class DatetimeModel(TunedModel):
    year: int
    month: int
    day: int


class ShowUser(TunedModel):
    id: int
    username: str
    amount: float
    last_promotion: DatetimeModel


class UserModel(BaseModel):
    username: str
    password: str
    amount: float
    last_promotion: DatetimeModel


class TokenModel(BaseModel):
    access_token: str
    token_type: str



